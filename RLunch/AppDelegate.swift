//
//  AppDelegate.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.initialSetup()
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return AuthService.handle(with: url, options: options)
    }

    func applicationWillTerminate(_ application: UIApplication) {
        CoreDataStack.shared.saveContext()
    }
    
    func resetRootViewController() {
        self.window?.rootViewController = nil
        self.setupRootViewController(viewController: LoginViewController.initFromNib())
    }
}

//MARK: Setup

private extension AppDelegate {
    
    func initialSetup() {
        self.setupAppearance()
        self.prefetchUser()
    }
    
    func setupRootViewController(viewController: UIViewController) {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
    }
    
    func setupAppearance() {
        UILabel.appearance().font = UIFont.boldSystemFont(ofSize: 22)
        UILabel.appearance().textColor = .white
        UITextView.appearance().textColor = .white
        UITextView.appearance().font = UIFont.systemFont(ofSize: 20)
        UITextView.appearance()
        UIBarButtonItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font: UIFont.systemFont(ofSize: 17)], for: .normal)
        UINavigationBar.appearance().tintColor = .white
    }
}

//MARK: Fetching

private extension AppDelegate {
    
    func prefetchUser() {
        User.fetch { [weak self] result in
            switch result {
            case .success(let user):
                let viewController = ViewControllerBuilder.initialViewController(by: user)
                self?.setupRootViewController(viewController: viewController)
            case .failure:
                self?.setupRootViewController(viewController: LoginViewController.initFromNib())
            }
        }
    }
}

