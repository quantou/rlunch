//
//  AppConstants.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation

struct AppConstants {
    static let UnknownErrorDescription = "Unknown error"
    static let ChooseNameTitle = "Please Choose your name"
    static let LogoutTitle = "Log out"
    static let EmptyOrderText = "Hope your weekend is going well;)"
}
