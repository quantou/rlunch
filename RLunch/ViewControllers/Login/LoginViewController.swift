//
//  LoginViewController.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit
import GoogleSignIn

final class LoginViewController: BaseViewController, GIDSignInUIDelegate {
    
    let authService = AuthService()
        
    @IBAction func googleAuthButtonDidTapped(_ sender: Any) {
        self.signUp()
    }
}

// MARK: - Actions

private extension LoginViewController {
    
    func signUp() {
        self.authService.signInWithGoogle(presentingController: self) { [weak self] result in
            switch result {
            case .success(let id):
                self?.saveUser(with: id)
            case .failure(let error):
                print(error)
            }
        }
    }
    
    func saveUser(with id: String) {
        User.createAndFetch(with: id, completion: { [weak self] result in
            switch result {
            case .success(let user):
                self?.presentNamePickerViewController(with: user)
            case .failure(let error):
                print(error)
            }
        })
    }
}

// MARK: - Navigation

private extension LoginViewController {
    
    func presentNamePickerViewController(with user: User) {
        let navigationController = ViewControllerBuilder.namePickerNavigationController(with:user)
        self.present(navigationController, animated: true, completion: nil)
    }
}
