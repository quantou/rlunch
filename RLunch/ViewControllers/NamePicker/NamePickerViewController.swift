//
//  NamePickerViewController.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/23/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

final class NamePickerViewController: BaseViewController {
    
    @IBOutlet weak private var tableView: UITableView!
    var user: User?
    private var names: [String]?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
}

//MARK: Setup

private extension NamePickerViewController {
    
    func initialSetup() {
        self.configureTableView()
        self.configureNavigationBar()
        self.configureNavigationTitle()
        self.title = AppConstants.ChooseNameTitle
        self.fetchNames()
    }
    
    func configureTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(NameTableViewCell.self, forCellReuseIdentifier: NameTableViewCell.reuseIdentifier)
    }
    
    func configureNavigationTitle() {
        self.title = AppConstants.ChooseNameTitle
    }
}

//MARK: UITableViewDataSource

extension NamePickerViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.names?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: NameTableViewCell.reuseIdentifier, for: indexPath) as! NameTableViewCell
        let name = names?[indexPath.row]
        cell.textLabel?.text = name
        return cell
    }
}

//MARK: UITableViewDelegate

extension NamePickerViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let name = names?[indexPath.row]
        self.updateUser(with: name, index: indexPath.row)
        self.pushUserLunchViewController()
    }
}

//MARK: Navigation

private extension NamePickerViewController {
    
    func pushUserLunchViewController() {
        let userLunchViewController = UserLunchViewController.initFromNib()
        userLunchViewController.user = user
        self.navigationController?.pushViewController(userLunchViewController, animated: true)
    }
}

private extension NamePickerViewController {
    
    func updateUser(with name: String?, index: Int) {
        self.user?.name = name
        self.user?.index = Int16(index)
        CoreDataStack.shared.saveContext()
    }
}

//MARK: Fetching

private extension NamePickerViewController {
    func fetchNames() {
        QueryService.shared.fetchUserNames { [weak self] result in
            switch result {
            case .success(let names):
                self?.names = names
                self?.tableView.reloadData()
            case .failure(let error):
                print(error)
            }
        }
    }
}
