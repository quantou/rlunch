//
//  BaseViewController.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func configureNavigationBar() {
        self.navigationController?.navigationBar.barStyle = .blackTranslucent
        self.navigationItem.hidesBackButton = true
    }
}
