//
//  UserLunchViewController.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/23/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

final class UserLunchViewController: BaseViewController {
    
    @IBOutlet weak fileprivate var nameLabel: UILabel!
    @IBOutlet weak fileprivate var orderTextView: UITextView!
    var user: User?
    private var weekSheet = WeekSheet(fromRawValue: Date().weekSymbol())
    private var order: Order?

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
}

//MARK: Setup

private extension UserLunchViewController {
    
    func initialSetup() {
        self.configureNavigationBar()
        self.configureNavigationTitle()
        self.configureNavigationItems()
        self.configureNameLabel()
        self.subscribeToNotification()
        self.fetchOrder()
    }
    
    func configureNavigationTitle() {
        self.title = "\(self.weekSheet.title)"
    }
    
    func configureTextView() {
        self.orderTextView.text = self.order?.text ?? AppConstants.EmptyOrderText
    }
    
    func configureNameLabel() {
        self.nameLabel.text = self.user?.name
    }
    
    func configureNavigationItems() {
        let logoutItem = UIBarButtonItem(title: AppConstants.LogoutTitle, style: .plain, target: self, action: #selector(self.logoutItemAction))
        self.navigationItem.rightBarButtonItem = logoutItem
    }
    
    func subscribeToNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(self.fetchOrder), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
}

//MARK: Actions

private extension UserLunchViewController {
    
    @objc func logoutItemAction() {
        AuthService().logOut { [weak self] _ in
            User.clear()
            self?.dismiss(animated:true, completion: nil)
            let appDelegate = UIApplication.shared.delegate as? AppDelegate
            appDelegate?.resetRootViewController()
        }
    }
}

//MARK: Fetching

private extension UserLunchViewController {
    
    @objc func fetchOrder() {
        guard let user = user,let sheetKey = self.weekSheet.key  else {
            return self.configureTextView()
        }
        QueryService.shared.fetchOrder(for: user, sheetKey: sheetKey) { [weak self] result in
            switch result {
            case .success(let order):
                self?.order = order
                self?.configureTextView()
            case .failure(let error):
                print(error)
            }
        }
    }
}
