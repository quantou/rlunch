//
//  Reusable.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/24/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation

protocol Reusable: class {
    static var reuseIdentifier: String {get}
}

extension Reusable {
    static var reuseIdentifier: String {
        return "\(String(describing: self))Identifier"
    }
}
