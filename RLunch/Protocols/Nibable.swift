//
//  Nibable.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation
import UIKit

protocol Nibable {
    static var nibName: String { get }
}

extension Nibable {
    
    static var nibName: String {
        return "\(String(describing: self))"
    }
}
