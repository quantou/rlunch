//
//  AuthService.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation
import GoogleSignIn
import GoogleAPIClientForREST

typealias Result = QueryResult<String>
typealias ResultCompletion = (QueryResult<String>) -> Void

// MARK: - QueryResult

enum QueryResult<Value> {
    
    case success(Value)
    case failure(Error)
    
    public var value: Value? {
        switch self {
        case .success(let value):
            return value
        case .failure:
            return nil
        }
    }
    
    var error: Error? {
        switch self {
        case .success:
            return nil
        case .failure(let error):
            return error
        }
    }
}

// MARK: - AuthService

final class AuthService: NSObject {
    
    private let CLIENT_ID = "409420595105-3p4t23n6o3enmi08l6tguhh4qbosibg7.apps.googleusercontent.com"
    private var signCompletion: ResultCompletion?
    private let scopes = [kGTLRAuthScopeSheetsSpreadsheetsReadonly]
    
    override init() {
        super.init()
        self.setup()
    }
    
    static func handle(with url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        return GIDSignIn.sharedInstance().handle(url as URL?,
                                          sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                          annotation: options[UIApplication.OpenURLOptionsKey.annotation])
    }
    
    func signInWithGoogle(presentingController: UIViewController, _ completion: @escaping ResultCompletion) {
        prepareForSignWithGoogle(presentingController, completion)
        GIDSignIn.sharedInstance().scopes = scopes
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().signIn()
    }
    
    func logOut(_ completion: @escaping ResultCompletion) {
        signCompletion = completion
        GIDSignIn.sharedInstance().signOut()
        GIDSignIn.sharedInstance().disconnect()
    }
}

private extension AuthService {
    
    func setup() {
        GIDSignIn.sharedInstance().clientID = CLIENT_ID
        GIDSignIn.sharedInstance().delegate = self
    }
    
    func didAuthOperation(_ result: Result) -> Void {
        self.signCompletion?(result)
        self.signCompletion = nil
    }
    
    func prepareForSignWithGoogle(_ presentingController: UIViewController, _ completion: @escaping ResultCompletion) {
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().uiDelegate = presentingController as? GIDSignInUIDelegate
        guard signCompletion == nil else {
            completion(.failure(AppError(errorDescription:"AuthService.prepareForSignWithGoogle: signCompletion == nil")))
            return
        }
        signCompletion = completion
    }
}

// MARK: - GIDSignInDelegate

extension AuthService: GIDSignInDelegate {
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        guard let id = user?.userID, error == nil else {
            self.didAuthOperation(.failure(error))
            return
        }
        self.didAuthOperation(.success(id))
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        guard error == nil else {
            self.didAuthOperation(.failure(error))
            return
        }
        self.didAuthOperation(.success("success"))
    }
}
