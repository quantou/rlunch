//
//  QueryService.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation
import GoogleAPIClientForREST

final class QueryService {
    
    static let shared = QueryService()
    private let spreadsheetId = "1NrPDjp80_7venKB0OsIqZLrq47jbx9c-lrWILYJPS88"
    
    private lazy var service: GTLRSheetsService = {
        let service = GTLRSheetsService()
        service.apiKey = "AIzaSyDBjL5hMiIbqSX-9IEFQ27hpLUY8F1sBzU"
        return service
    }()
    
    func fetchUserNames(completion: @escaping (QueryResult<[String]>) -> Void) {
        let range = "A3:A"
        let query = GTLRSheetsQuery_SpreadsheetsValuesGet
            .query(withSpreadsheetId: spreadsheetId, range:range)
        query.majorDimension = "ROWS"
        service.executeQuery(query) { (ticket, result, error) in
            if let error = error {
                return completion(.failure(error))
            }
            completion(QueryValueHandler.handledUserNames(result))
        }
    }
    
    func fetchOrder(for user: User, sheetKey: String, completion: @escaping (QueryResult<Order>) -> Void) {
        let query = GTLRSheetsQuery_SpreadsheetsGet
            .query(withSpreadsheetId: spreadsheetId)
        query.ranges = ["\(sheetKey)!B2:2","\(sheetKey)!B\(user.rowIndex):\(user.rowIndex)"]
        query.includeGridData = true
        
        service.executeQuery(query) { (ticket, result, error) in
            if let error = error {
                return completion(.failure(error))
            }
            completion(QueryValueHandler.handledOrderResult(result))
        }
    }
}

final class QueryValueHandler {
    
    static func handledOrderResult(_ result: Any?) -> QueryResult<Order> {
        guard let data = (result as? GTLRSheets_Spreadsheet)?.sheets?.first?.data,
            let dishes = data.first?.rowData?.first?.values?.map({$0.formattedValue}).compactMap({$0}),
            let values = data.last?.rowData?.first?.values
            else {
                let error = AppError(errorDescription: "QueryValueHandler.handledOrderResult: data = nil")
                return .failure(error)
        }
        let indexes = values.indices.filter({ values[$0].formattedValue == "1" })
        let orderList = indexes.map({dishes.indices.contains($0) ? dishes[$0] : nil}).compactMap{$0}
        let order = Order(with: orderList)
        return .success(order)
    }
    
    static func handledUserNames(_ result: Any?) -> QueryResult<[String]> {
        guard let results = (result as? GTLRSheets_ValueRange)?.values as? [[String]],
            let list = results.map({$0.first}) as? [String] else {
                let error = AppError(errorDescription: "QueryValueHandler.handledUserNames : results = nil")
                return .failure(error)
        }
        return .success(list)
    }
}
