//
//  CoreDataStack.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation
import CoreData

final class CoreDataStack {
    
    static let shared = CoreDataStack()
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "RLunch")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let error = (error as NSError).appError
                print("Unresolved error \(error)")
            }
        }
    }
        
    static func deleteData(with fetchRequest: NSFetchRequest<NSFetchRequestResult>) {
        fetchRequest.returnsObjectsAsFaults = false
        let managedContext = CoreDataStack.shared.persistentContainer.viewContext
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedContext.execute(deleteRequest)
            try managedContext.save()
        } catch {
            let error = (error as NSError).appError
            print("Unresolved error \(error)")
        }
    }
    
    //MARK: - Perform methods
    
    static func performBackgroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        CoreDataStack.shared.persistentContainer.performBackgroundTask(block)
    }
    
    static func performViewTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        block(CoreDataStack.shared.persistentContainer.viewContext)
    }
}
