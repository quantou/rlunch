//
//  ViewControllerBuilder.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/23/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

final class ViewControllerBuilder {
    
    static func initialViewController(by user: User) -> UIViewController {
        if user.name?.isEmpty ?? true {
            return ViewControllerBuilder.namePickerNavigationController(with:user)
        } else {
            return ViewControllerBuilder.userLunchNavigationController(with:user)
        }
    }
    
    static func namePickerNavigationController(with user: User?) -> UINavigationController {
        let namePickerViewController = NamePickerViewController.initFromNib()
        namePickerViewController.user = user
        let navigationController = UINavigationController(rootViewController: namePickerViewController)
        return navigationController
    }
    
    static func userLunchNavigationController(with user: User?) -> UINavigationController {
        let userLunchViewController = UserLunchViewController.initFromNib()
        userLunchViewController.user = user
        let navigationController = UINavigationController(rootViewController: userLunchViewController)
        return navigationController
    }
}
