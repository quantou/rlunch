//
//  GoogleAuthLabel.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

@IBDesignable public class GoogleAuthButton: UIButton {
    @IBInspectable var corner: CGFloat = 0.0 {
        didSet {
            self.roundCorners(by: self.corner)
        }
    }
}

