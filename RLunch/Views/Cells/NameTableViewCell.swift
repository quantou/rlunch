//
//  NameTableViewCell.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/24/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

class NameTableViewCell: UITableViewCell,Reusable {

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.handleState(selected)
    }
}

//MARK: Setup

private extension NameTableViewCell {
    
    func initialSetup() {
        self.configureCell()
    }
    
    func configureCell() {
        self.backgroundColor = .clear
        self.contentView.backgroundColor = .clear
        self.textLabel?.textColor = .white
        self.selectionStyle = .none
    }
}

//MARK: Handling Selected State

private extension NameTableViewCell {
    
    func checmarkImageView() -> UIImageView {
        let imageView = UIImageView(frame: CGRect(origin: CGPoint.zero, size: CGSize.init(width: 20, height: 20)))
        imageView.image = UIImage(named: "checkmark")
        imageView.backgroundColor = .clear
        return imageView
    }
    
    func handleState(_ selected: Bool) {
        if selected {
            self.accessoryView = self.checmarkImageView()
        } else {
            self.accessoryView = nil
        }
    }
}
