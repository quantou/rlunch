//
//  UIViewControllerExtensions.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

extension UIViewController: Nibable {
    
    static func initFromNib() -> Self {
        func instanceFromNib<T: UIViewController>() -> T {
            return T(nibName: T.nibName, bundle: nil)
        }
        return instanceFromNib()
    }
    
    
}
