//
//  UIViewExtensions.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import UIKit

extension UIView {
    
    func roundCorners(by radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
}
