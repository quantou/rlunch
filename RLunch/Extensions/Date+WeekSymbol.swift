//
//  DateExtension.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/25/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation

extension Date {
    
    func weekSymbol() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self)
    }
}
