//
//  User+CoreDataProperties.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/24/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//
//

import Foundation
import CoreData

extension User {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var index: Int16
    
    var rowIndex: Int {
        return Int(self.index + 3)
    }
    
    private static func create(with id: String, completion: @escaping (QueryResult<User>) -> Void) {
        CoreDataStack.performBackgroundTask { (context) in
            let obj = User(context: context)
            obj.id = id
            do {
                try context.save()
                DispatchQueue.main.async {
                    completion(QueryResult.success(obj))
                }
            } catch let error {
                DispatchQueue.main.async {
                    completion(QueryResult.failure(error))
                }
            }
        }
    }
    
    static func createAndFetch(with id: String, completion: @escaping (QueryResult<User>) -> Void) {
        User.create(with: id) { result in
            if let error = result.error {
                return completion(QueryResult.failure(error))
            }
            User.fetch(with: { result in
                completion(result)
            })
        }
    }
    
    static func clear() {
        CoreDataStack.deleteData(with: User.fetchRequest())
    }
    
    static func fetch(with completion: @escaping (QueryResult<User>) -> Void) {
        CoreDataStack.performViewTask { (context) in
            do {
                let response = try context.fetch(self.fetchRequest())
                guard let user = response.first as? User else {
                    let error = AppError(errorDescription: "User.fetch: User Is Not Exist")
                    return completion(QueryResult.failure(error))
                }
                completion(QueryResult.success(user))
            } catch let error {
                completion(QueryResult.failure(error))
            }
        }
    }

}
