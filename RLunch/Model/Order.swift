//
//  Order.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/23/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation

struct Order {
    
    var dishes = [Dish]()
    
    var text: String {
        var text = ""
        self.dishes.forEach{ dish in
            guard let title = dish.title else {return}
            text += title + "\n"
        }
        return text
    }
    
    init(with list: [String]) {
        list.forEach { title in
            self.dishes.append(Dish(title: title))
        }
    }
}
