//
//  WeekSheet.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/25/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation

enum WeekSheet: String {
    
    case monday
    case tuesday
    case wednesday
    case thursday
    case friday
    case weekend
    
    var key: String? {
        switch self {
        case .monday:
            return "'Понедельник '"
        case .tuesday:
            return "'Вторник '"
        case .wednesday:
            return "'Среда '"
        case .thursday:
            return "'Четверг '"
        case .friday:
            return "'Пятница '"
        default:
            return nil
        }
    }
    
    var title: String {
        return self.rawValue.capitalized + " Lunch"
    }
    
    init(fromRawValue: String){
        self = WeekSheet(rawValue: fromRawValue.lowercased()) ?? .weekend
    }
}
