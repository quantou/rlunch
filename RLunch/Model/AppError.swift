//
//  AppError.swift
//  RLunch
//
//  Created by Dimitri Kulakoff on 11/22/18.
//  Copyright © 2018 Dimitri Kulakoff. All rights reserved.
//

import Foundation

struct AppError: Error {
    var domain: String
    var code: Int
    var userInfo = [AnyHashable: Any]()
    
    init(domain: String? = "", code: Int? = 0, userInfo dictionary: [AnyHashable: Any]) {
        self.domain = domain!
        self.code = code!
        self.userInfo = dictionary
    }
    
    init(errorDescription: String) {
        self.init(userInfo: [NSLocalizedDescriptionKey: errorDescription])
    }
}

extension AppError {
    var errorDescription: String {
        if let errDesc = self.userInfo[NSLocalizedDescriptionKey] as? String {
            return errDesc
        }
        return AppConstants.UnknownErrorDescription
    }
}

extension Error {
    var appError: AppError {
        if self is AppError {
            return self as! AppError
        } else{
            let castedError = self as NSError
            return AppError(domain: castedError.domain, code: castedError.code, userInfo: castedError.userInfo)
        }
    }
}
